//
//  User.swift
//  money_keeper_ios
//
//  Created by PeCheRukiN on 21.06.17.
//  Copyright © 2017 PeCheRukiN. All rights reserved.
//

import Foundation

struct User {
    var id: String
    var name: String
    var email: String
    var password: String
}
