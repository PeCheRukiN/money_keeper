//
//  Debt.swift
//  money_keeper_ios
//
//  Created by PeCheRukiN on 21.06.17.
//  Copyright © 2017 PeCheRukiN. All rights reserved.
//

import Foundation

struct Debt {
    var walletId: String
    var value: Double
    var comment: String
    var createdAt: String
}
