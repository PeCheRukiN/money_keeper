//
//  Wallet.swift
//  money_keeper_ios
//
//  Created by PeCheRukiN on 21.06.17.
//  Copyright © 2017 PeCheRukiN. All rights reserved.
//

import Foundation

struct Wallet {
    var id: String
    var name: String
    var incomes: [Income]
    var consumptions: [Consumption]
    var debts: [Debt]
    var plans: [Plan]
}
