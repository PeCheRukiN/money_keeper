//
//  WalletChartController.swift
//  money_keeper_ios
//
//  Created by PeCheRukiN on 21.06.17.
//  Copyright © 2017 PeCheRukiN. All rights reserved.
//

import UIKit
import Charts

class WalletChartController: UIViewController {

    @IBOutlet weak var chartView: BarChartView!
    
    weak var axisFormatDelegate: IAxisValueFormatter?
    
    var wallet: Wallet!
    var summIncomes: Double = 0
    var summConsumptions: Double = 0
    var summDebt: Double = 0
    var summPlans: Double = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        axisFormatDelegate = self
        wallet = Wallet(id: "", name: "", incomes: [Income(walletId: "", value: 200, comment: "", createdAt: "")], consumptions: [Consumption(walletId: "", value: 70, comment: "", createdAt: "")], debts: [], plans: [])
        
        configureDataSourse()
        updateChartView()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func configureDataSourse() {
        summIncomes = 0
        summDebt = 0
        summPlans = 0
        summConsumptions = 0
        
        for income in wallet.incomes {
            summIncomes += income.value
        }
        for consumption in wallet.consumptions {
            summConsumptions += consumption.value
        }
        for debt in wallet.debts {
            summDebt += debt.value
        }
        for plan in wallet.plans {
            summIncomes += plan.value
        }
    }
    
    
    func updateChartView() {
        var dataEntries: [PieChartDataEntry] = []
        let income = PieChartDataEntry(value: summIncomes)
        let consumption = PieChartDataEntry(value: summConsumptions)
        let plan = PieChartDataEntry(value: summPlans)
        let debt = PieChartDataEntry(value: summDebt)
        dataEntries = [income, consumption, plan]
        let chartDataSet = PieChartDataSet(values: dataEntries, label: "Fucking fuck")
        let chartData = PieChartData(dataSet: chartDataSet)
        chartView.data = chartData
        
        let xaxis = chartView.xAxis
        xaxis.valueFormatter = axisFormatDelegate
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension WalletChartController: IAxisValueFormatter {
    
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm.ss"
        return dateFormatter.string(from: Date(timeIntervalSince1970: value))
    }
    
}
